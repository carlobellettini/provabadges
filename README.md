
# https://badgen.net

![Badge giallo](https://badgen.net/badge/Prova/Testo/yellow) fisso:
`![Badge giallo](https://badgen.net/badge/Prova/Testo/yellow)`


https://badgen.net/gitlab

![Badge giallo](https://badgen.net/gitlab/last-commit/carlobellettini/provabadges?cache=300) di gitlab:
`![Badge giallo](https://badgen.net/gitlab/last-commit/carlobellettini/provabadges)`

# https://shields.io

![Badge giallo](https://img.shields.io/badge/Prova-Testo-yellow)
`https://img.shields.io/badge/Prova-Testo-yellow`

![Badge giallo](https://img.shields.io/gitlab/coverage/carlobellettini/provabadges/main?logo=gitlab)

![Comandi](https://gitlab.com/carlobellettini/provabadges/-/jobs/artifacts/main/raw/badges2/copComm.svg?job=test)
![Condizioni](https://gitlab.com/carlobellettini/provabadges/-/jobs/artifacts/main/raw/badges2/copCond.svg?job=test)

# anybadge

![Comandi](https://gitlab.com/carlobellettini/provabadges/-/jobs/artifacts/main/raw/badges/copComm.svg?job=anybadge)
![Condizioni](https://gitlab.com/carlobellettini/provabadges/-/jobs/artifacts/main/raw/badges/copCond.svg?job=anybadge)

