package vec;

public class Prova {
  public int m1(int a) {
    switch (a) {
      case 0: return 1;
      case 1: return 2;
      case 2: return 4;
      case 3: return 8;
      default: return 16;
    }
  }
}
