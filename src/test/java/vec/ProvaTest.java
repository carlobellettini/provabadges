package vec;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class ProvaTest {

  @ParameterizedTest
  @ValueSource(ints={0,1,2,3,4})
  void m1(int exp) {
    Prova SUT = new Prova();
    assertThat(SUT.m1(exp)).isEqualTo((int)Math.pow(2,exp));
  }
}