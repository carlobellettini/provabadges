# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Gradle.gitlab-ci.yml

# This is the Gradle build system for JVM applications
# https://gradle.org/
# https://github.com/gradle/gradle

image: gradle:alpine

# Disable the Gradle daemon for Continuous Integration servers as correctness
# is usually a priority over speed in CI environments. Using a fresh
# runtime for each build is more reliable since the runtime is completely
# isolated from any previous builds.

before_script:
  - GRADLE_USER_HOME="$(pwd)/.gradle"
  - export GRADLE_USER_HOME

stages:
  - build
  - test
  - badges
  - deploy

build:
  stage: build
  script:
    - gradle --build-cache assemble
  cache:
    key: "$CI_COMMIT_REF_NAME"
    policy: push
    paths:
      - build
      - .gradle

test:
  stage: test
  script:
    - apk add libxml2-utils
    - gradle check jacocoTestReport
    - mkdir badges2
    - export comandi=$(xmllint --html build/reports/jacoco/test/html/index.html --xpath "//tfoot//td[3]/text()" )
    - export condizioni=$(xmllint --html build/reports/jacoco/test/html/index.html --xpath "//tfoot//td[5]/text()" )
    - wget -O badges2/copComm.svg http://shields.io/badge/Copertura_comandi-${comandi}25-green
    - wget -O badges2/copCond.svg http://shields.io/badge/Copertura_condizioni-${condizioni}25-green
  coverage: '/    - Instruction Coverage: ([0-9.]+)%/'
  artifacts:
    reports:
      junit: build/test-results/test/**/TEST-*.xml
    paths:
      - "build/reports/tests/test/"
      - "build/reports/jacoco/test/html/"
      - "badges2/"
    expire_in: 30 days
  cache:
    key: "$CI_COMMIT_REF_NAME"
    paths:
      - build
      - .gradle

coverage:
  stage: deploy
  image: registry.gitlab.com/haynes/jacoco2cobertura:1.0.7
  script:
    - python /opt/cover2cover.py build/reports/jacoco/test/jacocoTestReport.xml src/main/java > build/cob.xml # read the <source></source> tag and prepend the path to every filename attribute
    - python /opt/source2filename.py build/cob.xml
  dependencies:
    - test
  cache:
    key: "$CI_COMMIT_REF_NAME"
    policy: pull
    paths:
      - build
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: build/cob.xml

badges:
  stage: badges
  script:
    - mkdir datiCoverage
    - echo '{"subject":"copertura comandi", "status":"40%", "color":"blue", "schemaVersion":1, "label":"copertura comandi", "message":"40%"}' > datiCoverage/comandi.json
    - echo '{"color":"blue", "schemaVersion":1, "label":"copertura comandi", "message":"40%"}' > datiCoverage/comandi2.json
  artifacts:
    expose_as: 'miei json'
    paths:
      - "datiCoverage/"
    expire_in: 7 days

anybadge:
  stage: badges
  image: python:3.6.6
  script:
    - pip install anybadge lxml
    - mkdir badges
    - export comandi=$(python -c "from lxml import html; print(html.parse('build/reports/jacoco/test/html/index.html').xpath(\"//table[@id='coveragetable']/tfoot/tr/td[3]/text()\")[0])")
    - export condizioni=$(python -c "from lxml import html; print(html.parse('build/reports/jacoco/test/html/index.html').xpath(\"//table[@id='coveragetable']/tfoot/tr/td[5]/text()\")[0])")
    - anybadge -l "AB Copertura comandi" -v ${comandi%%%} coverage > badges/copComm.svg
    - anybadge -l "AB Copertura condizioni" -v ${condizioni%%%} coverage > badges/copCond.svg
  artifacts:
    paths:
      - badges/
    when: always
    expire_in: 4 weeks